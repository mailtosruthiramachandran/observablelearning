import { Component, OnInit } from '@angular/core';
import { Observable, map, filter, of, from, Subscriber } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  names$ !: Observable<string[]>;

    ngOnInit() {
      const myObservable$ = new Observable((observer) => {
        observer.next('1');
        observer.next('2');
        observer.complete();
        observer.error('Found the error');
      })

      myObservable$.subscribe((val) => {
        console.log(val);
      }, (error) => {
        console.log(error);  
      }, () => {
        console.log('observable is completed');
        
      })

      const observable1$ = Observable.create((observer : Subscriber<any>) => {
        observer.next('1');
        observer.next('2');
        observer.error('Found the error');
      })

      observable1$.subscribe((val: string) => {
        console.log(val);
      }, (error: Error) => {
        console.log(error);  
      }, () => {
        console.log('observable is completed');   
      })

      const observable2$ = from([1,2,3,4,5]); //takes single argument and iterates and promise can be converted to observable
    
      observable2$.pipe(
        filter(i => i >= 3),
        map(i => i + 3)
      ).subscribe((data) => {
        console.log(data);
      }, (error: Error)=> {
        console.log(error);
      }, () => {
        console.log('completed');
      })

      const observable3$ = of([1,2,3,4,5], [ 'A' , 'B', 'C']); // takes multiple arguments, don't iterate as from

      observable3$.subscribe((data) => {
        console.log(data);
      }, (error: Error) => {
        console.log(error); 
      }, () => {
        console.log('completed');
        
      })

      this.names$ = of(['sruthi', 'ramesh', 'saru', 'mathura', 'arthi']);

      
    }
}
